package utils

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"strings"
	"testing"
)

func TestConvertHexToBase64(t *testing.T) {
	hex := "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	decoded := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	if !strings.EqualFold(decoded, ConvertHexToBase64(hex)) {
		t.Fatalf("Failed to decode hex to base 64")
	} else {
		fmt.Println("Hex decoded")
	}
}

func TestXorHexStrings(t *testing.T) {
	hex1 := "1c0111001f010100061a024b53535009181c"
	hex2 := "686974207468652062756c6c277320657965"
	exp := "746865206b696420646f6e277420706c6179"
	res, err := XorHexStrings(hex1, hex2)
	if err != nil {
		t.Fatalf("%v", err)
	}
	if !strings.EqualFold(res, exp) {
		t.Fatalf("Bad xor of hex strings")
	} else {
		fmt.Println("Hex strings xor'ed")
	}
}

func TestXorBytes(t *testing.T) {
	message := []byte("Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal")
	key := []byte("ICE")
	exp, _ := hex.DecodeString("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f")
	out := XorBytes(message, key)
	if !bytes.Equal(exp, out) {
		t.Fatalf("Repeating key xor failed")
	}
	fmt.Println("Repeating key Xor works")
}

func TestHammingDistance(t *testing.T) {
	distance := HammingDistance([]byte("this is a test"), []byte("wokka wokka!!!"))
	if distance != 37 {
		t.Fatalf("Hamming Distance wrong")
	} else {
		fmt.Println("Hamming Distance was", distance)
	}
}

func TestFindBestXor(t *testing.T) {
	enc, _ := hex.DecodeString("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")
	key, best := FindBestXor(enc)
	fmt.Printf("Key: %v, %s\n", key, string(best))
}

func TestScoreText(t *testing.T) {
}
