package utils

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math"
	"os"
	"strings"
)

func ConvertHexToBase64(encoded string) string {
	hex, err := hex.DecodeString(encoded)
	if err != nil {
		fmt.Println("Unable to decode hex")
		os.Exit(1)
	}

	decoded := base64.StdEncoding.EncodeToString(hex)

	return decoded
}

func XorHexStrings(block1 string, block2 string) (string, error) {
	if len(block1) != len(block2) {
		return "", fmt.Errorf("String length does not match")
	}
	binary1, _ := hex.DecodeString(block1)
	binary2, _ := hex.DecodeString(block2)
	res := XorBytes(binary1, binary2)
	return hex.EncodeToString(res), nil
}

// The second argument is repeated to match the length of the first argument
func XorBytes(b1 []byte, b2 []byte) []byte {
	res := make([]byte, len(b1))
	for i := range b1 {
		res[i] = b1[i] ^ b2[i%len(b2)]
	}
	return res
}

/*func transpose(ciphertexts [][]byte) [][]byte{

}*/

func HammingDistance(b1, b2 []byte) int {
	totalDifference := 0
	if len(b1) != len(b2) {
		fmt.Printf("Length mismatch")
	}

	for i := range b1 {
		diff := b1[i] ^ b2[i]
		for j := 0; j < 8; j++ {
			totalDifference += int(diff % 2)
			diff >>= 1
		}
	}
	return totalDifference
}

// Takes raw bytes of ciphertext (not hex encoded or anything)
func FindBestXor(b []byte) (key byte, best string) {
	bestScore := math.Inf(1)
	encByte := byte(0)
	bestString := ""
	i := 0
	for ; i < 256; i++ {
		text := string(XorBytes(b, []byte{byte(i)}))
		newScore := ScoreText(text)
		if newScore < bestScore {
			bestScore = newScore
			bestString = text
			encByte = byte(i)
		}
	}

	return encByte, bestString
}

// Scores strings on how english they are (assumes ASCII) a-z and space
// Lower Scores are better. Best scores are around 100
func ScoreText(s string) float64 {
	b := []byte(strings.ToLower(s))
	sLen := len(b)
	frequencies := []float64{0.0651738, 0.0124248, 0.0217339, 0.0349835, 0.1041442, 0.0197881,
		0.0158610, 0.0492888, 0.0558094, 0.0009033, 0.0050529, 0.0331490, 0.0202124, 0.0564513,
		0.0596302, 0.0137645, 0.0008606, 0.0497563, 0.0515760, 0.0729357, 0.0225134, 0.0082903,
		0.0171272, 0.0013692, 0.0145984, 0.0007836, 0.1918182} //Space is at the end
	counts := make([]int, 27, 27)
	invalid := 0

	chi2 := 0.0
	for _, c := range b {
		if c >= 65 && c <= 90 {
			counts[c-65]++
		} else if c >= 97 && c <= 122 {
			counts[c-97]++
		} else if c == 32 {
			counts[26]++
		} else {
			invalid++
			chi2 += float64(sLen)
		}
	}
	/*	if invalid > sLen/2 {
		return math.Inf(1)
	}*/

	for i := range frequencies {
		obs := float64(counts[i])
		exp := frequencies[i] * float64(sLen)
		chi2 += ((obs - exp) * (obs - exp)) / exp
	}
	return chi2
}
