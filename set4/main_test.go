package set4

import (
	"bitbucket.org/sebsebmc/cryptopals/set1"
	"bitbucket.org/sebsebmc/cryptopals/set2"
	"bytes"
	"encoding/base64"
	"io/ioutil"
	"testing"
)

func TestAESCtrEdit(t *testing.T) {
	cts, err := ioutil.ReadFile("../set1/chal7.txt")
	if err != nil {
		t.Fatalf("Cannot read file")
	}
	cts, err = base64.StdEncoding.DecodeString(string(cts))
	if err != nil {
		t.Fatalf("Invalid base64")
	}
	pt := make([]byte, len(cts))
	decrypter := set1.NewECBDecrypter([]byte("YELLOW SUBMARINE"))
	decrypter.CryptBlocks(pt, cts)
	ct, edit := makeCtrEditApi(pt)
	pt2 := breakCtrEdit(ct, edit)
	if !bytes.Equal(pt2, pt) {
		t.Fatalf("Mismatched plaintext")
	}
}

func TestCtrBitflip(t *testing.T) {
	genCookie, isAdmin := createCTRCookieOracle()
	adminCookie := CtrBitflip(genCookie)
	if !isAdmin(adminCookie) {
		t.Fatalf("Did not create admin cookie")
	}
}

func TestKeyFromIV(t *testing.T) {
	key, enc, dec := createCBCKeyAsIVOracle()
	key2 := recoverCBCKeyFromIV(enc, dec)
	if !bytes.Equal(key, key2) {
		t.Fatalf("Keys did not match\n %v\n %v", key, key2)
	}
}

func TestSHA1KeyedMAC(t *testing.T) {
	key := set2.GetAESKey()
	h := SHA1KeyedMAC(key, bytes.Repeat([]byte("test"), 16))
	mac := (*h).Sum([]byte(nil))
	(*h).Write([]byte{1})
	mac2 := (*h).Sum([]byte(nil))
	if bytes.Equal(mac, mac2) {
		t.Fatalf("Somehow got the same mac")
	}
}

func TestSHA1LengthExtension(t *testing.T) {
	key := set2.GetAESKey()
	msg := bytes.Repeat([]byte("test"), 16)
	h := SHA1KeyedMAC(key, msg)
	suffix := []byte(";admin=true")
	mac := SHA1LengthExtension(suffix, h)
	msg = append(msg, suffix...)
	if !validateMAC(key, msg, mac) {
		t.Fatalf("Failed to extend mac")
	}
}
