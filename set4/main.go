package set4

import (
	"bitbucket.org/sebsebmc/cryptopals/set2"
	"bitbucket.org/sebsebmc/cryptopals/set3"
	"bitbucket.org/sebsebmc/cryptopals/utils"
	"bytes"
	"crypto/aes"
	"crypto/sha1"
	"hash"
	"log"
)

func makeCtrEditApi(pt []byte) (ct []byte, edit func([]byte, int, []byte) []byte) {
	//Fix the key using closures
	key := set2.GetAESKey()
	iv := set2.GetAESKey()
	b, _ := aes.NewCipher(key)
	enc := set3.NewAesCtr(b, iv)

	ct = make([]byte, len(pt))
	enc.XorKeyStream(ct, []byte(pt))

	edit = func(ct []byte, offset int, newText []byte) []byte {
		enc := set3.NewAesCtr(b, iv)
		decBuf := make([]byte, len(ct))
		enc.XorKeyStream(decBuf, ct)
		copy(decBuf[offset:offset+len(newText)], newText)
		res := make([]byte, len(decBuf))
		enc = set3.NewAesCtr(b, iv)
		enc.XorKeyStream(res, decBuf)
		return res
	}
	return
}

func breakCtrEdit(ct []byte, edit func([]byte, int, []byte) []byte) []byte {
	empty := make([]byte, len(ct))
	ct2 := edit(ct, 0, empty)
	pt := utils.XorBytes(ct, ct2)
	return pt
}

func createCTRCookieOracle() (getCookie func(string) []byte, isAdmin func([]byte) bool) {
	key := set2.GetAESKey()
	b, _ := aes.NewCipher(key)
	iv := set2.GetAESKey()
	encrypter := set3.NewAesCtr(b, iv)
	prefix := []byte("comment1=cooking%20MCs;userdata=")
	suffix := []byte(";comment2=%20like%20a%20pound%20of%20bacon")

	getCookie = func(userData string) []byte {
		pt := make([]byte, 0)
		pt = append(pt, prefix...)
		quotedData := bytes.Replace([]byte(userData), []byte(";"), []byte("%3B"), -1)
		quotedData = bytes.Replace(quotedData, []byte("="), []byte("%3D"), -1)
		pt = append(pt, quotedData...)
		pt = append(pt, suffix...)
		ct := make([]byte, len(pt))
		encrypter.XorKeyStream(ct, pt)
		return ct
	}
	isAdmin = func(cookie []byte) bool {
		decrypter := set3.NewAesCtr(b, iv)
		pt := make([]byte, len(cookie))
		decrypter.XorKeyStream(pt, cookie)
		return bytes.Contains(pt, []byte(";admin=true;"))
	}
	return
}

func CtrBitflip(getCookie func(string) []byte) []byte {
	userData := "AAAAAAAAAAAAAAAA"
	ct := getCookie(userData)
	copy(ct[32:48], utils.XorBytes(ct[32:48], []byte(userData)))           //XOR with A's to null the bytes
	copy(ct[32:48], utils.XorBytes(ct[32:48], []byte("AAAAA;admin=true"))) //Set the bytes to the target string
	return ct
}

func createCBCKeyAsIVOracle() (key []byte, enc func([]byte) []byte, dec func([]byte) (bool, []byte)) {
	key = set2.GetAESKey()
	encrypter := set2.NewCBCEncrypter(key, key)
	decrypter := set2.NewCBCDecrypter(key, key)

	enc = func(userData []byte) []byte {
		pt := make([]byte, 0)
		padded := set2.Pkcs7Pad(pt, 16)
		ct := make([]byte, len(padded))
		encrypter.CryptBlocks(ct, padded)
		return ct
	}
	dec = func(cookie []byte) (bool, []byte) {
		pt := make([]byte, len(cookie))
		decrypter.CryptBlocks(pt, cookie)
		for i := 0; i < len(pt); i++ {
			if pt[i] > 127 {
				return false, pt
			}
		}
		return true, nil
	}
	return
}

func recoverCBCKeyFromIV(enc func([]byte) []byte, dec func([]byte) (bool, []byte)) []byte {
	ct := enc(bytes.Repeat([]byte("A"), 48))
	modifiedCt := make([]byte, 48)
	copy(modifiedCt[:16], ct[:16])
	copy(modifiedCt[32:], ct[:16])
	success, pt := dec(modifiedCt)
	if success {
		log.Printf("Valid plaintext?")
	}
	key := utils.XorBytes(pt[:16], pt[32:])
	return key
}

func SHA1KeyedMAC(key, message []byte) *hash.Hash {
	s := sha1.New()
	s.Write(key)
	s.Write(message)
	return &s
}

func validateMAC(key, message, mac []byte) bool {
	s := sha1.New()
	s.Write(key)
	s.Write(message)
	mac2 := s.Sum([]byte(nil))
	if bytes.Equal(mac, mac2) {
		return true
	}
	return false
}

func SHA1LengthExtension(message []byte, hash2 *hash.Hash) []byte {
	(*hash2).Write(message)
	return (*hash2).Sum([]byte(nil))
}
