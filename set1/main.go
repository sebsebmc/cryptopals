package set1

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"math"

	"bitbucket.org/sebsebmc/cryptopals/utils"
)

func FindBestLine(lines [][]byte) string {
	bestLine := ""
	bestScore := math.Inf(1)

	for _, l := range lines {
		_, text := utils.FindBestXor(l)
		score := utils.ScoreText(text)
		if score < bestScore {
			bestScore = score
			bestLine = text
		}
	}
	return bestLine
}

func BreakRepeatingKeyXor(ct []byte) []byte {
	//Guess the key size
	bestDist := 8.1 // Impossible max distance for a byte (since we normalize)
	bestSize := 2
	for i := 2; i < len(ct)/8; i++ {
		dist := utils.HammingDistance(ct[:i*4], ct[i*4:i*8])
		//dist += utils.HammingDistance(ct[i*2:i*3], ct[i*3:i*4])
		normDist := float64(dist) / float64(i*4)
		if normDist < bestDist {
			fmt.Printf("%d, %f\n", i, normDist)
			bestDist = normDist
			bestSize = i
		}
	}
	//Split text into keysize blocks
	stacks := make([][]byte, bestSize)
	for i := 0; i < bestSize; i++ {
		stacks[i] = make([]byte, 0)
	}
	for i := 0; i < len(ct); {
		for j := 0; j < bestSize && i < len(ct); j++ {
			stacks[j] = append(stacks[j], ct[i])
			i += 1
		}
	}
	key := make([]byte, bestSize)
	for i := 0; i < bestSize; i++ {
		key[i], _ = utils.FindBestXor(stacks[i])
	}
	fmt.Printf("Key: %s\n", string(key))
	pt := utils.XorBytes(ct, key)
	return pt
}

func DetectECB(ct []byte) int {
	if len(ct) < 32 {
		panic("Cannot detect without at least 2 blocks")
	}
	for i := 0; i < len(ct)-16; i += 16 {
		for j := i + 16; j < len(ct); j += 16 {
			if bytes.Equal(ct[i:i+16], ct[j:j+16]) {
				return i
			}
		}
	}
	return -1
}

type ECBEncrypter struct {
	b cipher.Block
}

type ECBDecrypter ECBEncrypter

func (ecb ECBEncrypter) BlockSize() int {
	return ecb.b.BlockSize()
}

func (ecb ECBDecrypter) BlockSize() int {
	return ecb.b.BlockSize()
}

func NewECBEncrypter(key []byte) ECBEncrypter {
	b, _ := aes.NewCipher(key)
	enc := ECBEncrypter{b}
	return enc
}

func NewECBDecrypter(key []byte) ECBDecrypter {
	b, _ := aes.NewCipher(key)
	enc := ECBDecrypter{b}
	return enc
}

func (ecb ECBEncrypter) CryptBlocks(dst, src []byte) {
	if len(dst) < len(src) {
		panic("Dest buffer to small")
	}
	step := ecb.b.BlockSize()
	for i := 0; i < len(src); i += step {
		if i+step >= len(src) {
			ecb.b.Encrypt(dst[i:], src[i:])
		} else {
			ecb.b.Encrypt(dst[i:i+step], src[i:i+step])
		}
	}
}

func (ecb ECBDecrypter) CryptBlocks(dst, src []byte) {
	if len(dst) < len(src) {
		panic("Dest buffer too small")
	}
	step := ecb.b.BlockSize()
	for i := 0; i < len(src); i += step {
		if i+step >= len(src) {
			ecb.b.Decrypt(dst[i:], src[i:])
		} else {
			ecb.b.Decrypt(dst[i:i+step], src[i:i+step])
		}
	}
}
