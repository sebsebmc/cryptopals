package set1

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestFindBestLine(t *testing.T) {
	file, err := os.Open("./chal4.txt")
	if err != nil {
		t.Errorf("Invalid file name")
	}
	defer file.Close()
	lines := make([][]byte, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		decoded, err := hex.DecodeString(scanner.Text())
		if err != nil {
			log.Fatal("Could not decode hex string")
		}
		lines = append(lines, decoded)
	}
	best := FindBestLine(lines)
	fmt.Println(best)
}

func TestBreakRepeatingKeyXor(t *testing.T) {
	file, err := os.Open("./chal6.txt")
	if err != nil {
		t.Fatalf("Invalid file name")
	}
	defer file.Close()
	ct, err := ioutil.ReadAll(file)
	if err != nil {
		t.Fatalf("Could not read ciphertext")
	}
	ct, err = base64.StdEncoding.DecodeString(string(ct))
	if err != nil {
		t.Fatalf("Invalid base64")
	}
	pt := BreakRepeatingKeyXor(ct)
	fmt.Printf("%s\n", string(pt))
}

func TestEcbDecrypt(t *testing.T) {
	cts, err := ioutil.ReadFile("./chal7.txt")
	if err != nil {
		t.Fatalf("Cannot read file")
	}
	cts, err = base64.StdEncoding.DecodeString(string(cts))
	if err != nil {
		t.Fatalf("Invalid base64")
	}
	b, _ := aes.NewCipher([]byte("YELLOW SUBMARINE"))
	pt := make([]byte, len(cts))
	decrypter := ECBDecrypter{b}
	decrypter.CryptBlocks(pt, cts)
	fmt.Printf("%s\n", pt)
}

func TestEcbDetection(t *testing.T) {
	cts, err := ioutil.ReadFile("./chal8.txt")
	if err != nil {
		t.Fatalf("Cannot read file")
	}
	lines := bytes.Split(cts, []byte("\n"))
	for _, line := range lines {
		if len(line) == 0 {
			continue //Empty line for last line
		}
		decoded, err := hex.DecodeString(string(line))
		if err != nil {
			t.Fatalf("Invalid hex: %s\n", line)
		}
		if DetectECB(decoded) >= 0 {
			fmt.Printf("Found line: %s\n", decoded)
		}
	}
}
