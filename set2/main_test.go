package set2

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/url"
	"testing"
)

func TestPkcs7Pad(t *testing.T) {
	in := []byte("YELLOW SUBMARINE")
	exp := []byte("YELLOW SUBMARINE\x04\x04\x04\x04")
	out := Pkcs7Pad(in, 20)
	if !bytes.Equal(exp, out) {
		t.Fatalf("Padding failure %#v", out)
	}
	fmt.Println("Pad successful")
}

func TestCBCDecrypt(t *testing.T) {
	ct, err := ioutil.ReadFile("./10.txt")
	if err != nil {
		t.Fatalf("Failed to read file 10.txt")
	}
	ct, err = base64.StdEncoding.DecodeString(string(ct))
	if err != nil {
		t.Fatalf("Unable to decode base64")
	}
	ct = Pkcs7Pad(ct, 16)
	dec := NewCBCDecrypter([]byte("YELLOW SUBMARINE"), []byte("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"))
	dst := make([]byte, len(ct))
	dec.CryptBlocks(dst, ct)
	fmt.Printf("%s", string(dst))
}

func TestCBCTestVector(t *testing.T) {
	key, _ := hex.DecodeString("4278b840fb44aaa757c1bf04acbe1a3e")
	iv, _ := hex.DecodeString("57f02a5c5339daeb0a2908a06ac6393f")
	plain, _ := hex.DecodeString("3c888bbbb1a8eb9f3e9b87acaad986c466e2f7071c83083b8a557971918850e5")
	cipher, _ := hex.DecodeString("479c89ec14bc98994e62b2c705b5014e175bd7832e7e60a1e92aac568a861eb7")
	dec := NewCBCDecrypter(key, iv)
	testpt := make([]byte, len(plain))
	dec.CryptBlocks(testpt, cipher)
	if !bytes.Equal(testpt, plain) {
		t.Fatalf("Failed to decrypt test vector\n")
	}
}

func TestBlockModeOracle(t *testing.T) {
	for i := 0; i < 100; i++ {
		enc, isECB := blackBoxEncrypter()
		guess := blockModeOracle(enc)
		if isECB != guess {
			t.Fatal("Failed to detect ECB properly")
		}
	}
}

func TestECBDecrypter(t *testing.T) {
	secretB64 := "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
	secret, _ := base64.StdEncoding.DecodeString(secretB64)
	secret = Pkcs7Pad(secret, 16)
	suffixEncoder := secretSuffixEncoder()
	dec := ECBPrefixSuffixDecrypter(suffixEncoder)
	fmt.Printf("%s\n", dec)
	// Because the decryption changes the number of bytes in the message, the strategy doesn't
	// work without a special case for padding, which changes based on length... :(
	// So we see if there is less than one block of difference at the end of the messages
	t.Logf("%d, %d\n", len(secret), len(dec))
	if len(secret) != len(dec) {
		t.Fatalf("Messages aren't equal length")
	}
	matching := 0
	for i := range secret {
		if secret[i] == dec[i] {
			matching++
		} else {
			break
		}
	}
	//Due to padding...
	if matching > len(secret)-16 {
		t.Logf("Message decrypted: %s\n", string(dec))
	} else {
		t.Fatalf("Did not match enough bytes")
	}
}

func TestMakeCookie(t *testing.T) {
	valid := createCookie("example@example.com")
	t.Logf("%s", valid)
	val, _ := url.ParseQuery(valid)
	if val.Get("email") != "example@example.com" {
		t.Fatalf("Did not get same email back from cookie")
	}
	notAdmin := createCookie("example@example.com&admin=true")
	val, _ = url.ParseQuery(notAdmin)
	if val.Get("role") == "admin" {
		t.Fatalf("did not escape email properly")
	}
}

func TestCookieOracle(t *testing.T) {
	gen, isAdmin := createECBCookieOracle()
	if isAdmin(gen("&role=admin")) {
		t.Fatalf("Able to generate admin cookie trivially")
	}
}

func TestECBCutPaste(t *testing.T) {
	gen, isAdmin := createECBCookieOracle()
	adminCookie := ECBCutPaste(gen)
	if !isAdmin(adminCookie) {
		t.Fatalf("Did not create an admin cookie")
	}
}

func TestDetectPrefixLen(t *testing.T) {
	encryptFunc := secretSuffixPrefixEncoder()
	prefixLen := detectECBPrefixLen(encryptFunc, 16)
	if prefixLen < 0 {
		t.Fatalf("Prefix length not found")
	}
	t.Logf("Prefix length: %d", prefixLen)
}

func TestECBPrefixSuffixDecrypter(t *testing.T) {
	secretB64 := "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
	secret, _ := base64.StdEncoding.DecodeString(secretB64)
	secret = Pkcs7Pad(secret, 16)
	sfxPfxEncoder := secretSuffixPrefixEncoder()
	dec := ECBPrefixSuffixDecrypter(sfxPfxEncoder)

	// Because the decryption changes the number of bytes in the message, the strategy doesn't
	// work without a special case for padding, which changes based on length... :(
	// So we see if there is less than one block of difference at the end of the messages
	t.Logf("%d, %d\n", len(secret), len(dec))
	if len(secret) != len(dec) {
		t.Fatalf("Messages aren't equal length")
	}
	matching := 0
	for i := range secret {
		if secret[i] == dec[i] {
			matching++
		} else {
			break
		}
	}
	//Due to padding...
	if matching > len(secret)-16 {
		t.Logf("Message decrypted: %s\n", string(dec))
	} else {
		t.Fatalf("Did not match enough bytes")
	}
}

func TestPKCS7Unpad(t *testing.T) {
	_, err := PKCS7Unpad([]byte("ICE ICE BABY\x04\x04\x04\x04"))
	if err != nil {
		t.Fatalf("Valid padding erred")
	}
	_, err = PKCS7Unpad([]byte("ICE ICE BABY\x05\x05\x05\x05"))
	if err == nil {
		t.Fatalf("Padding is invalid length")
	}
	_, err = PKCS7Unpad([]byte("ICE ICE BABY\x01\x02\x03\x04"))
	if err == nil {
		t.Fatalf("Padding bytes are not identical")
	}
}

func TestCreateCBCOracle(t *testing.T) {
	gen, isAdmin := createCBCCookieOracle()
	cookie := gen(";admin=true;")
	if isAdmin(cookie) {
		t.Fatalf("Trvivally creates admin cookies")
	}
}

func TestCBCBitflip(t *testing.T) {
	gen, isAdmin := createCBCCookieOracle()
	adminCookie := CBCBitflip(gen)
	if !isAdmin(adminCookie) {
		t.Fatalf("Did not create an admin cookie")
	}
}
