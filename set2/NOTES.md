## How to keep the algorithm elegant

The input string is "A" * blockSize + "Secret string..."
You only control the first block so we have to keep cycling over the first block length from blockSize
down to 0.

After solving the first block of the secret string we build our dictionaries using the decrypted first
block and figure out the second block but still by manipulating the length of our prefix string of "A"s
In other words, we are always modifying the prefix block, for the first block we use a bunch of A's.
For the second block we make our prefix block equal to the first block and we change the last byte then
use a prefix thats blockSize-1 long and check what the first byte of the second block is. Repeat for
each block.


## Last Block
There is probably a more efficient way to solve the last block since we know the padding scheme,
but to keep things generic we should proceed the same way as the other blocks, only the last byte
of the last block may be weird.

### Pseudocode
```
pfxLen = 16
dict = map[[]byte]byte
decrypted = []byte
blocks = X
blockSize = Y
curBlock = 1
curByte = blockSize - 1

for blocks {
	for bytes {
		//First build the dictionary
		//Empty the dictionary
			make(dict)
		if curBlock = 1
			prefix = A * curByte
		else //block > 1
			copy(prefix, solved[block-1])
		//Assuming ASCII in the 0-127
		for i = 0; i < 128; i++ {
			dict[enc(prefix)[curBlock:curBlock+1]] = i
		}
		prefix = prefix[:pfxLen-1]
		out = enc(prefix)
		k, ok := dict[out]
		if !ok {
			panic
		}
		decrypted[curBlock+curByte] = k
	}
}
```
## Cut and Paste
For the Cut and paste challenge, we need to block align the interesting parts that we want to stitch together
`role=admin` has a space so we need to split into `role=` or `=admin` so that we can legally insert those into a cookie

Then we splice them together with some other chunks we get from valid cookies
```
email=exampl%40e
xample.com&role=
user&uid=51
```
So we can get `&role=` from using exampl@example.com
```
email=aaaaaexamp
le%40example.com
admin&role=user&
uid=51
``` 
This email gives us `admin&role=user&` which is actually really nice because we should be able to reuse the last
block now and in the real world keep the correct uid

Stitching it together we should be able to do:
```
email=exampl%40e
xample.com&role=
admin&role=user&
uid=51
```
And code in the real world would most likely just use the first occurrance of admin, so we get to become admin
This would fail in the real world if they did good email validation... 

## Prefixed ECB decryption
We just want to reduce this down to the suffix case, as which point we can refactor the suffix code to take a start
index/block and make sure to pad out the prefix to a block boundary. Easy...

To do that, we want to find the length of the prefix so that we can can always just prepend enough bytes to our input
strings that we use in the suffix case. Then we can reuse the suffix code (with a bit of refactoring)

### Detecting prefix length
We know that if we input 3 identical blocks we are guaranteed 2 identical blocks in the output if there was a prefix.
To detect the actual length of the prefix we can start by putting in 2 blocks and see if that results in identical 
output blocks. We keep adding 1 more character to the input until we get the 2 identical blocks of output

## CBC Bitflipping
As a refresher, [CBC](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_Block_Chaining_(CBC)) encrypts
by taking the ciphertext of the previous block and encrypting it with the plaintext of the next block before it is 
encrypted. So if we want to get the string `;admin=true;` into the cookie indirectly we can try XOR'ing the right
bytes together so that the final cookie ends up with our attack string. Of course, this will result in the block that we
becoming completely scrambled.

```
0123456789abcdef <- Random IV
comment1=cooking
%20MCs;userdata=
AAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAA
;comment2=%20lik
e%20a%20pound%20
of%20bacon
```
Because we know what we put into the target area, we know what to XOR the previous ciphertext block with.

```
    CIPHERTEXT BLOCK
    AAAAAAAAAAAAAAAA
XOR .....;admin=true
```
(we're just taking advantage of the fact that prefix is 32 characters so we don't need to add the final `;` ourselves)