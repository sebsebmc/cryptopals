package set2

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	mathrand "math/rand"
	"net/url"
	"strconv"
	"time"

	"bitbucket.org/sebsebmc/cryptopals/set1"
	"bitbucket.org/sebsebmc/cryptopals/utils"
)

//Pads a buffer to PKCS#7 padding as per https://en.wikipedia.org/wiki/Padding_(cryptography)#PKCS7
func Pkcs7Pad(msg []byte, size int) []byte {
	toPad := 0
	if len(msg)%size == 0 {
		toPad = size
	} else {
		toPad = size - (len(msg) % size)
	}
	padded := make([]byte, len(msg)+toPad)
	copy(padded, msg)
	for i := len(msg); i < len(padded); i++ {
		padded[i] = byte(toPad)
	}
	return padded
}

type CbcEncrypter struct {
	b    cipher.Block
	prev []byte
}

type CbcDecrypter CbcEncrypter

func NewCBCDecrypter(key, iv []byte) CbcDecrypter {
	b, _ := aes.NewCipher(key)
	prev := make([]byte, b.BlockSize())
	copy(prev, iv)
	return CbcDecrypter{b, prev}
}

func NewCBCEncrypter(key, iv []byte) CbcEncrypter {
	b, _ := aes.NewCipher(key)
	prev := make([]byte, b.BlockSize())
	copy(prev, iv)
	return CbcEncrypter{b, prev}
}

func (cbc CbcEncrypter) BlockSize() int {
	return cbc.b.BlockSize()
}

func (cbc CbcDecrypter) BlockSize() int {
	return cbc.b.BlockSize()
}

//Pass in padded plaintext
func (cbc CbcEncrypter) CryptBlocks(dst, src []byte) {
	if len(dst) < len(src) {
		panic("Dest buffer to small")
	}
	step := cbc.b.BlockSize()
	if len(src)%step != 0 {
		panic("Input length is not multiple of block size\n")
	}
	for i := 0; i < len(src); i += step {
		if i+step >= len(src) {
			copy(src[i:], utils.XorBytes(src[i:], cbc.prev))
			cbc.b.Encrypt(dst[i:], src[i:])
			copy(cbc.prev, dst[i:])
		} else {
			copy(src[i:i+step], utils.XorBytes(src[i:i+step], cbc.prev))
			cbc.b.Encrypt(dst[i:i+step], src[i:i+step])
			copy(cbc.prev, dst[i:i+step])
		}
	}
}

func (cbc CbcDecrypter) CryptBlocks(dst, src []byte) {
	if len(dst) < len(src) {
		panic("Dest buffer too small")
	}
	step := cbc.b.BlockSize()
	tmp := make([]byte, step)
	for i := 0; i < len(src); i += step {
		if i+step >= len(src) {
			//copy(tmp, src[i:]) Can be left out in the last block for efficiency...
			cbc.b.Decrypt(dst[i:], src[i:])
			copy(dst[i:], utils.XorBytes(dst[i:], cbc.prev))
			//copy(cbc.prev, tmp)
		} else {
			copy(tmp, src[i:i+step])
			cbc.b.Decrypt(dst[i:i+step], src[i:i+step])
			copy(dst[i:i+step], utils.XorBytes(dst[i:i+step], cbc.prev))
			copy(cbc.prev, tmp)
		}
	}
}

func GetAESKey() []byte {
	k := make([]byte, 16)
	rand.Read(k)
	return k
}

type encrypter func([]byte) []byte

//Builds a closure that does either ECB or CBC and returns the closure and whether it is ECB or not
func blackBoxEncrypter() (encrypter, bool) {
	r := mathrand.New(mathrand.NewSource(time.Now().UnixNano()))
	pre := r.Intn(5) + 5
	post := r.Intn(5) + 5

	key := make([]byte, 16)
	r.Read(key)
	//Choose ECB or CBC
	//If CBC generate random IV
	if r.Intn(2) == 1 {
		return func(in []byte) []byte {
			buf := make([]byte, pre+len(in)+post)
			r.Read(buf[:pre])
			r.Read(buf[len(buf)-post:])
			copy(buf[pre:pre+len(in)], in)
			pt := Pkcs7Pad(buf, 16)
			enc := set1.NewECBEncrypter(key)
			ct := make([]byte, len(pt))
			enc.CryptBlocks(ct, pt)
			return ct
		}, true
	} else {
		return func(in []byte) []byte {
			buf := make([]byte, pre+len(in)+post)
			r.Read(buf[:pre])
			r.Read(buf[len(buf)-post:])
			copy(buf[pre:pre+len(in)], in)
			pt := Pkcs7Pad(buf, 16)
			iv := make([]byte, 16)
			r.Read(iv)
			enc := NewCBCEncrypter(key, iv)
			ct := make([]byte, len(pt))
			enc.CryptBlocks(ct, pt)
			return ct
		}, false
	}
}

//Returns true if deteced ECB, false if CBC
func blockModeOracle(enc encrypter) bool {
	// Our plaintext needs to be 3 blocks to see repitition in the ECB output
	pt := make([]byte, 16*3) // We can just use the 0 value
	ct := enc(pt)
	return set1.DetectECB(ct) >= 0
}

func secretSuffixEncoder() encrypter {
	secretB64 := "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
	secret, _ := base64.StdEncoding.DecodeString(secretB64)
	post := len(secret)
	key := make([]byte, 16)
	rand.Read(key)

	return func(in []byte) []byte {
		buf := make([]byte, len(in)+post)
		copy(buf, in)
		copy(buf[len(in):], secret)
		pt := Pkcs7Pad(buf, 16)
		enc := set1.NewECBEncrypter(key)
		ct := make([]byte, len(pt))
		enc.CryptBlocks(ct, pt)
		return ct
	}
}

func secretSuffixPrefixEncoder() encrypter {
	pfxLen := mathrand.Intn(20) + 10
	//fmt.Printf("prefix: %d\n", pfxLen)
	prefix := make([]byte, pfxLen)
	mathrand.Read(prefix)
	//fmt.Println(prefix)
	suffixEncoder := secretSuffixEncoder()
	return func(in []byte) []byte {
		prefixedIn := make([]byte, pfxLen+len(in))
		copy(prefixedIn, prefix)
		copy(prefixedIn[len(prefix):], in)
		return suffixEncoder(prefixedIn)
	}
}

func getBlockSize(enc encrypter) int {
	baseOutput := enc([]byte{})
	initSize := len(baseOutput)
	blockSize := 1
	//Determine block size
	for {
		testStr := bytes.Repeat([]byte{0x40}, blockSize)
		newSize := len(enc(testStr))
		if newSize != initSize {
			blockSize = newSize - initSize
			break
		} else {
			blockSize += 1
		}
	}
	fmt.Printf("Detected Block Size: %d\n", blockSize)
	return blockSize
}

func ECBPrefixSuffixDecrypter(enc encrypter) []byte {
	isECB := blockModeOracle(enc)
	if !isECB {
		panic("Encryption function is not ECB")
	}

	baseOutput := enc([]byte{})
	initSize := len(baseOutput)
	blockSize := getBlockSize(enc)
	prefixLen := detectECBPrefixLen(enc, blockSize)
	fmt.Println(prefixLen)
	if prefixLen < 0 {
		panic("Did not find prefix length")
	}

	var skipLen int
	//Round up if not on a block boundary
	if prefixLen%16 != 0 {
		skipLen = ((prefixLen / blockSize) + 1) * blockSize
	} else {
		skipLen = prefixLen
	}
	padLen := skipLen - prefixLen

	//Find the last byte by building a dictionary of strings where the last character varies
	//Then when we compare it to the output of bringing the first byte into the previous block
	numBlks := (initSize / blockSize) - (skipLen / blockSize)
	fmt.Println(numBlks)
	decrypted := make([]byte, numBlks*blockSize)
	for curBlk := 0; curBlk < numBlks; curBlk++ {
		for curByte := blockSize - 1; curByte >= 0; curByte-- {

			absByte := (curBlk * blockSize) + (blockSize - curByte - 1)
			//Make a new dictionary to keep dictionary size small at the cost of more alloc calls
			dict := make(map[string]byte)
			sample := make([]byte, blockSize+padLen)
			//Create the sample buffer
			if curBlk == 0 {
				copy(sample, bytes.Repeat([]byte{0x41}, curByte+padLen))
				copy(sample[curByte+padLen:], decrypted[curBlk*blockSize:curBlk*blockSize+(blockSize-curByte)])
			} else {
				copy(sample[padLen:], decrypted[absByte-(blockSize-1):absByte])
			}

			for i := byte(0); i < 128; i++ {
				sample[padLen+blockSize-1] = i
				t := hex.EncodeToString(enc(sample)[skipLen : skipLen+blockSize])
				dict[t] = i
			}

			prefix := bytes.Repeat([]byte{0x41}, curByte+padLen)
			out := hex.EncodeToString(enc(prefix)[curBlk*blockSize+skipLen : (curBlk+1)*blockSize+skipLen])

			b, ok := dict[out]
			if !ok {
				fmt.Printf("Failed at byte: %d\n", absByte)
			} else {
				decrypted[curBlk*blockSize+(blockSize-curByte-1)] = b
			}
		}
	}
	return decrypted
}

func detectECBPrefixLen(enc encrypter, blockSize int) int {
	for i := 0; i < blockSize; i++ {
		ct := enc(bytes.Repeat([]byte{0x40}, blockSize*2+i))
		prefix := set1.DetectECB(ct)
		if prefix >= 0 {
			return prefix - i
		}
	}
	return -1
}

func PKCS7Unpad(padded []byte) ([]byte, error) {
	bufLen := len(padded)
	padCount := padded[bufLen-1]

	if int(padCount) > bufLen {
		return nil, fmt.Errorf("Invalid padding. Too large %d\n", padCount)
	}
	if padCount == 0 {
		return nil, fmt.Errorf("0 is not a valid padding\n")
	}

	for i := 0; i < int(padCount); i++ {
		if padded[bufLen-i-1] != padCount {
			return nil, fmt.Errorf("Invalid padding\n")
		}
	}
	//fmt.Printf("Valid with %d\n", padCount)
	return padded[:bufLen-int(padCount)], nil
}

func createCookie(email string) string {
	cookie := url.Values{}
	cookie.Add("email", email)
	//Getting a 2 digit id. This is just so its consistent
	cookie.Add("uid", strconv.Itoa(mathrand.Intn(90)+10))
	cookie.Add("role", "user")
	return cookie.Encode()
}

func createECBCookieOracle() (getCookie func(string) []byte, isAdmin func([]byte) bool) {
	//Fix the key using closures
	key := GetAESKey()
	encrypter := set1.NewECBEncrypter(key)
	decrypter := set1.NewECBDecrypter(key)

	getCookie = func(email string) []byte {
		cookieStr := createCookie(email)
		paddedCookie := Pkcs7Pad([]byte(cookieStr), 16)
		encCookieBuf := make([]byte, len(paddedCookie))
		encrypter.CryptBlocks(encCookieBuf, paddedCookie)
		return encCookieBuf
	}
	isAdmin = func(encCookie []byte) bool {
		decCookie := make([]byte, len(encCookie))
		decrypter.CryptBlocks(decCookie, encCookie)
		cookieStr := string(decCookie)
		vals, err := url.ParseQuery(cookieStr)
		if err != nil {
			panic("Cannot decrypt cookie")
		}
		return vals.Get("role") == "admin"
	}
	return
}

func createCBCCookieOracle() (getCookie func(string) []byte, isAdmin func([]byte) bool) {
	key := GetAESKey()
	iv := GetAESKey()
	encrypter := NewCBCEncrypter(key, iv)
	decrypter := NewCBCDecrypter(key, iv)
	prefix := []byte("comment1=cooking%20MCs;userdata=")
	suffix := []byte(";comment2=%20like%20a%20pound%20of%20bacon")

	getCookie = func(userData string) []byte {
		pt := make([]byte, 0)
		pt = append(pt, prefix...)
		quotedData := bytes.Replace([]byte(userData), []byte(";"), []byte("%3B"), -1)
		quotedData = bytes.Replace(quotedData, []byte("="), []byte("%3D"), -1)
		pt = append(pt, quotedData...)
		pt = append(pt, suffix...)
		padded := Pkcs7Pad(pt, 16)
		ct := make([]byte, len(padded))
		encrypter.CryptBlocks(ct, padded)
		return ct
	}
	isAdmin = func(cookie []byte) bool {
		pt := make([]byte, len(cookie))
		decrypter.CryptBlocks(pt, cookie)
		return bytes.Contains(pt, []byte(";admin=true;"))
	}
	return
}

func ECBCutPaste(getCookie func(string) []byte) []byte {
	roleEmail := "exampl@example.com"
	adminEmail := "aaaaaexample@example.comadmin"
	roleCookie := getCookie(roleEmail)
	adminCookie := getCookie(adminEmail)
	finalCookie := make([]byte, 16*4)
	copy(finalCookie[:32], roleCookie[:32])
	copy(finalCookie[32:], adminCookie[32:])
	return finalCookie
}

func CBCBitflip(getCookie func(string) []byte) []byte {
	userData := "AAAAAAAAAAAAAAAA"
	ct := getCookie(userData + userData)
	copy(ct[32:48], utils.XorBytes(ct[32:48], []byte(userData)))           //XOR with A's to null the bytes
	copy(ct[32:48], utils.XorBytes(ct[32:48], []byte(".....;admin=true"))) //Set the bytes to the target string
	return ct
}
