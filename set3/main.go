package set3

import (
	"bitbucket.org/sebsebmc/cryptopals/set1"
	"bitbucket.org/sebsebmc/cryptopals/set2"
	"bitbucket.org/sebsebmc/cryptopals/utils"
	"bytes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	mathrand "math/rand"
	"time"
)

func createCBCPaddingOracle(lines [][]byte) (genMessage func() []byte,
	checkPadding func([]byte) bool) {
	key := set2.GetAESKey()

	genMessage = func() []byte {
		idx := mathrand.Intn(len(lines))
		padded := set2.Pkcs7Pad(lines[idx], 16)
		out := make([]byte, len(padded))
		iv := make([]byte, 16)
		mathrand.Read(iv)
		enc := set2.NewCBCEncrypter(key, iv)
		enc.CryptBlocks(out, padded)

		return append(iv, out...)
	}
	checkPadding = func(ct []byte) bool {
		iv2 := ct[:16]
		ct2 := ct[16:]
		dec := set2.NewCBCDecrypter(key, iv2)
		out := make([]byte, len(ct2))
		dec.CryptBlocks(out, ct2)
		_, err := set2.PKCS7Unpad(out)
		if err != nil {
			return false
		}
		return true
	}
	return
}

func CBCPaddingOracle(ct []byte, checkPadding func([]byte) bool) []byte {
	solved := make([]byte, len(ct)-16)

	solveLastBlock := func(ct []byte) []byte {
		paddingByte := byte(1)
		testBuf := make([]byte, len(ct))
		copy(testBuf, ct)
		solved := make([]byte, 16)
		ctLen := len(ct) - 1
		for idx := 0; idx < 16; idx++ {
			// So here we create the padding for all the bytes we've solved so far
			for i := 0; i < int(paddingByte-1); i++ {
				//fmt.Println(ctLen-16-i)
				testBuf[ctLen-16-i] = ct[ctLen-16-i] ^ solved[15-i] ^ paddingByte
			}
			for i := 0; i < 256; i++ {
				// i is the byte we are assuming the plaintext is, and the padding byte is
				// what we want it to become
				testBuf[ctLen-idx-16] = ct[ctLen-idx-16] ^ byte(i) ^ paddingByte
				ok := checkPadding(testBuf)
				if ok {
					//fmt.Printf("%c\n", byte(i))
					solved[15-idx] = byte(i)
				}
			}
			paddingByte++
		}
		return solved
	}
	for i := len(ct); i > 16; i -= 16 {
		copy(solved[i-32:i-16], solveLastBlock(ct[:i]))
	}
	msg, _ := set2.PKCS7Unpad(solved)
	return msg
}

type Ctr struct {
	b     cipher.Block
	nonce uint64
	cnt   uint64
	buf   []byte
}

//iv is 16 bytes, the first 8 are the nonce, the last 8 are the counter
//TODO: the counter should start at 0?
func NewAesCtr(b cipher.Block, iv []byte) Ctr {
	return Ctr{
		b,
		binary.LittleEndian.Uint64(iv[:8]),
		binary.LittleEndian.Uint64(iv[8:]),
		make([]byte, 0),
	}
}

func (c Ctr) XorKeyStream(dest []byte, src []byte) {
	if len(dest) < len(src) {
		panic("dest too small\n")
	}
	//If we had a buf we need to use that first
	rem := len(c.buf)
	if rem > 0 {
		copy(dest[:rem], utils.XorBytes(c.buf, src[:rem]))
	}
	in := make([]byte, 16)
	i := rem
	for ; i < len(src)-c.b.BlockSize(); i += c.b.BlockSize() {
		c.buf = make([]byte, c.b.BlockSize())
		binary.LittleEndian.PutUint64(in[:8], c.nonce)
		binary.LittleEndian.PutUint64(in[8:], c.cnt)
		c.cnt++
		c.b.Encrypt(c.buf, in)
		copy(dest[i:i+c.b.BlockSize()], utils.XorBytes(src[i:i+c.b.BlockSize()], c.buf))
	}
	//If we have less than a block left over...
	if i < len(src) {
		c.buf = make([]byte, c.b.BlockSize())
		binary.LittleEndian.PutUint64(in[:8], c.nonce)
		binary.LittleEndian.PutUint64(in[8:], c.cnt)
		c.cnt++
		c.b.Encrypt(c.buf, in)
		copy(dest[i:len(src)], utils.XorBytes(src[i:], c.buf))
		c.buf = c.buf[:(len(src) - i)]
	}
}

func breakFixedNonceCTR(cts [][]byte) [][]byte {
	//find shortest length
	min := 4096
	for i := range cts {
		if len(cts[i]) < min {
			min = len(cts[i])
		}
	}
	fmt.Printf("Smallest block len: %d\n", min)
	concat := make([]byte, min*len(cts))
	//truncate them all to that size
	//concatenate together
	for i := 0; i < len(cts); i++ {
		copy(concat[i*min:(i+1)*min], cts[i][:min])
	}
	//feed to set1.BreakRepeatingKeyXor
	final := set1.BreakRepeatingKeyXor(concat)
	//Split back up
	pts := make([][]byte, len(cts))
	for i := 0; i < len(cts); i++ {
		pts[i] = final[i*min : (i+1)*min]
	}
	return pts
}

type MT19937Rand struct {
	state [624]uint32
	index int
}

func NewMT19937Rand(seed int) *MT19937Rand {
	mt := MT19937Rand{index: 624}
	mt.Seed(seed)
	return &mt
}

func (mt *MT19937Rand) Seed(seed int) {
	const f = 1812433253
	mt.state[0] = uint32(seed)
	for i := uint32(1); i < 624; i++ {
		mt.state[i] = f*(mt.state[i-1]^(mt.state[i-1]>>30)) + i
	}
	mt.twist()
}

func (mt *MT19937Rand) ExtractNumber() int32 {
	const u, d = uint32(11), uint32(0xFFFFFFFF)
	const s, b = uint32(7), uint32(0x9D2C5680)
	const t, c = uint32(15), uint32(0xEFC60000)
	const l = uint32(18)
	if mt.index >= 624 {
		mt.twist()
	}
	y := mt.state[mt.index]
	y = y ^ ((y >> u) & d)
	y = y ^ ((y << s) & b)
	y = y ^ ((y << t) & c)
	y = y ^ (y >> l)

	mt.index += 1
	return int32(y)
}

func (mt *MT19937Rand) twist() {
	const lowerMask = uint32(0x7FFFFFFF)
	const upperMask = ^lowerMask
	for i := 0; i < 624; i++ {
		x := (mt.state[i] & upperMask) + ((mt.state[(i+1)%624]) & lowerMask)
		xA := x >> 1
		if x%2 != 0 {
			xA = xA ^ 0x9908B0DF
		}
		mt.state[i] = mt.state[(i+397)%624] ^ xA
	}
	mt.index = 0
}

//Pass in a mt state so we can test on it (dependency injection)
func randSeedTimestamp(mt *MT19937Rand) int32 {
	offset := mathrand.Intn(1000) + mathrand.Intn(1000) + 80
	ts := time.Now().Second()
	mt.Seed(ts + offset)
	return mt.ExtractNumber()
}

func findTimestampSeed(output int32) int {
	ts := time.Now().Second()
	ts -= 500
	for i := ts; i < ts+3000; i++ {
		mt := NewMT19937Rand(i)
		if mt.ExtractNumber() == output {
			return i
		}
	}
	return -1
}

// We intentionally only provide a reference to the ExtractNumber method to the function
// to protect ourselves from cheating and to generalize this to any function that gets
// outputs from a MT19937 PRNG
func recoverMTState(nextRand func() int32) *MT19937Rand {
	mt := NewMT19937Rand(0)
	for i := 0; i < 624; i++ {
		n := nextRand()
		//fmt.Println()
		mt.state[i] = untemper(uint32(n))
	}
	mt.twist()
	return mt
}

func untemper(n uint32) uint32 {
	//Undoing: y = y ^ (y >> 18)
	top18 := n & 0xFFFFC000
	temp := n ^ (top18 >> 18)

	//Undoing: y = y ^ ((y << 15) & 0xEFC60000)
	bot15 := temp & 0x7FFF
	temp = temp ^ ((bot15 << 15) & 0xEFC60000)
	mid2 := temp & 0x18000
	temp = temp ^ ((mid2 << 15) & 0xEFC60000)

	//Undoing: y = y ^ ((y << 7) & 0x9D2C5680)
	bot7 := temp & 0x7F
	temp = temp ^ ((bot7 << 7) & 0x9D2C5680)
	next7 := temp & 0x3F80
	temp = temp ^ ((next7 << 7) & 0x9D2C5680)
	next7 = temp & 0x1FC000
	temp = temp ^ ((next7 << 7) & 0x9D2C5680)
	next7 = temp & 0xFE00000
	temp = temp ^ ((next7 << 7) & 0x9D2C5680)

	//Undoing: y = y ^ (y << 11)
	top11 := temp & 0xFFE00000
	temp = temp ^ (top11 >> 11)
	next11 := temp & 0x1FFC00
	temp = temp ^ (next11 >> 11)
	return temp
}

type MTCtr struct {
	mt        *MT19937Rand
	buf       []byte
	blockSize int
}

// 2^19937
func NewMT19937CTR(seed uint32) MTCtr {
	bs := 32 / 8
	return MTCtr{NewMT19937Rand(int(seed)),
		make([]byte, 0),
		bs}
}

func (c MTCtr) XorKeyStream(dest []byte, src []byte) {
	if len(dest) < len(src) {
		panic("dest too small\n")
	}
	//MT returns a 32-bit value, but we need byte level granularity
	blockSize := 32 / 8
	//If we had a buf we need to use that first
	rem := len(c.buf)
	if rem > 0 {
		copy(dest[:rem], utils.XorBytes(c.buf, src[:rem]))
	}
	i := rem
	for ; i < len(src)-blockSize; i += blockSize {
		c.buf = make([]byte, blockSize)
		binary.LittleEndian.PutUint32(c.buf, uint32(c.mt.ExtractNumber()))
		copy(dest[i:i+blockSize], utils.XorBytes(src[i:i+blockSize], c.buf))
	}
	//If we have less than a block left over...
	if i < len(src) {
		c.buf = make([]byte, blockSize)
		binary.LittleEndian.PutUint32(c.buf, uint32(c.mt.ExtractNumber()))
		copy(dest[i:len(src)], utils.XorBytes(src[i:], c.buf))
		c.buf = c.buf[:(len(src) - i)]
	}
}

func makeMTCtrMessage(msg []byte) []byte {
	prefix := make([]byte, mathrand.Intn(10)+5)
	mathrand.Read(prefix)
	msg = append(prefix, msg...)
	key := make([]byte, 4)
	//Terrible hack incoming
	_, err := rand.Read(key)
	if err != nil {
		fmt.Println(err)
	}
	key16bit := binary.LittleEndian.Uint32(key)
	key16bit >>= 16
	mtctr := NewMT19937CTR(key16bit)
	out := make([]byte, len(msg))
	mtctr.XorKeyStream(out, msg)
	return out
}

func recoverMTCtrKey(known, ct []byte) uint32 {
	diff := len(ct) - len(known)
	out := make([]byte, len(ct))
	known = append(bytes.Repeat([]byte{0}, diff), known...)

	for i := 0; i < (1 << 16); i++ {
		mtctr := NewMT19937CTR(uint32(i))
		mtctr.XorKeyStream(out, known)
		if bytes.Equal(ct[diff:], out[diff:]) {
			return uint32(i)
		}
	}
	panic("No key matched")
}
