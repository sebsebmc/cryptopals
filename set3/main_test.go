package set3

import (
	"bytes"
	"crypto/aes"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
	"time"
)

func getFileContents(filename string, t *testing.T) []byte {
	file, err := os.Open(filename)
	if err != nil {
		t.Fatalf("Invalid file name")
	}
	defer file.Close()
	ct, err := ioutil.ReadAll(file)
	if err != nil {
		t.Fatalf("Could not read ciphertext")
	}
	return ct
}

func TestCBCPaddingOracle(t *testing.T) {
	text := getFileContents("./17.txt", t)
	decLines := splitAndDecode(text, t)
	//Is the iv the first output block?
	enc, check := createCBCPaddingOracle(decLines)
	for i := 0; i < 10; i++ {
		msg := enc()
		if !check(msg) {
			t.Fatalf("Valid message fails padding oracle")
		}
		pt := CBCPaddingOracle(msg, check)
		if !bytes.Equal(pt[:5], []byte("00000")) {
			t.Fatalf("Prefix did not match")
		}
	}
}

func splitAndDecode(text []byte, t *testing.T) [][]byte {
	lines := bytes.Split(text, []byte("\n"))
	decLines := make([][]byte, len(lines))
	for i, line := range lines {
		out := make([]byte, len(line))
		count, err := base64.StdEncoding.Decode(out, line)
		if err != nil {
			t.Fatalf("Cannot decode base64\n")
		}
		decLines[i] = out[:count]
	}
	return decLines
}

func TestCTRDecryption(t *testing.T) {
	msg, err := base64.StdEncoding.DecodeString("L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==")
	if err != nil {
		t.Fatalf("Bad base64 msg")
	}
	b, _ := aes.NewCipher([]byte("YELLOW SUBMARINE"))
	ctr := NewAesCtr(b, bytes.Repeat([]byte{0}, 16))
	pt := make([]byte, len(msg))
	ctr.XorKeyStream(pt, msg)
	if !bytes.Equal(pt, []byte("Yo, VIP Let's kick it Ice, Ice, baby Ice, Ice, baby ")) {
		t.Fatalf("Does not match CTR")
	}
}

func TestFixedNonceCTR(t *testing.T) {
	text := getFileContents("./20.txt", t)
	cts := splitAndDecode(text, t)
	pts := breakFixedNonceCTR(cts)
	for i := range pts {
		fmt.Printf("%s\n", pts[i])
	}

}

func TestNewMT19937Rand(t *testing.T) {
	expected := []int32{-795755684, 581869302, -404620562, -708632711, 545404204, -133711905, -372047867, 949333985, -1579004998, 1323567403}
	mt := NewMT19937Rand(5489)
	for i := range expected {
		actual := int32(mt.ExtractNumber())
		if actual != expected[i] {
			t.Errorf("Failed on input %d, expected %d got %d\n", i, expected[i], actual)
		}
	}
}

func TestRecoverTimestampSeed(t *testing.T) {
	mt := NewMT19937Rand(0)
	out := randSeedTimestamp(mt)
	seed := findTimestampSeed(out)
	if seed == -1 {
		t.Fatalf("Failed to find a seed")
	}
	mt2 := NewMT19937Rand(seed)
	mt2.ExtractNumber()
	if mt2.ExtractNumber() != mt.ExtractNumber() {
		t.Fatalf("Next output failed to match")
	}
}

func TestRecoverMTState(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	mt := NewMT19937Rand(rand.Int())
	mt2 := recoverMTState(mt.ExtractNumber)
	for i := 0; i < 1000; i++ {
		if mt.ExtractNumber() != mt2.ExtractNumber() {
			t.Fatalf("Mismatched output")
		}
	}
}

func TestMTCtr(t *testing.T) {
	key := rand.Int31()
	enc := NewMT19937CTR(uint32(key))
	dec := NewMT19937CTR(uint32(key))
	for i := 1; i < 100; i++ {
		input := bytes.Repeat([]byte("A"), i)
		output := make([]byte, len(input))
		enc.XorKeyStream(output, input)
		if bytes.Equal(input, output) {
			t.Errorf("Encryption does nothing?")
		}
		res := make([]byte, len(output))
		dec.XorKeyStream(res, output)
		if !bytes.Equal(input, res) {
			t.Errorf("Dec(Enc(m)) != m, len: %d, res: %v\n", i, res)
		}
	}
}

func TestMTCtrKeyRecovery(t *testing.T) {
	msg := bytes.Repeat([]byte("A"), 14)
	ct := makeMTCtrMessage(msg)
	key := recoverMTCtrKey(msg, ct)
	t.Logf("Key: %d\n", key)
}
