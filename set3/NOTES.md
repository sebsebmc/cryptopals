## Padding Oracle
In set2 we saw how with CBC you can modify a a block by XOR'ing
the previous ciphertext block. In this case we also have a function
that tells us if the padding is valid for a ciphertext.

We can attempt to create our own valid paddings and see what values
we had to XOR the last bit with in order to figure out the value of
the last bit. Once we know the last bit we can work backwards to try
and create a valid 2 byte padding, etc.

We look at 2 blocks at a time. The block we XOR and the block whose
plaintext will result in the padding attack.

## MT19937 Recovery
We receive output from the random number generator that is the result of
```go
y := mt.state[mt.index]
y = y ^ (y >> 11)
y = y ^ ((y << 7) & 0x9D2C5680)
y = y ^ ((y << 15) & 0xEFC60000)
y = y ^ (y >> 18)
mt.index++
```
so we need to undo theses steps in reverse order and we should then have
the internal state of the original PRNG. We set the index to 625 and the
outputs should be identical.

### Reversing the mutations
```
32 bits
0123456789012345 6789012345678901
yyyyyyyyyyyyyyyy yyyyyyyyyyyyyyyy
0000000000000000 00yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
```
So to reverse this we already have the top bits just fine, we need to
XOR the bottom bits with the top bits we have to reverse it
```
32 bits
0123456789012345 6789012345678901
yyyyyyyyyyyyyyyy y000000000000000

&
‭1110111111000110 0000000000000000‬
```
This time the shift is less than half the bits of the number, so we 
need to apply the xor multiple times with different intermediate values
to get the result.

Then we still need to handle the AND operation.

Now that we know how to undo bot types of operations we just need to do
the similar things to the previous 2 operations as well (the next one 
really sucks because its only a shift by 7 so we have to repeat 5 times)